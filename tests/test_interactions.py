import unittest

from ..models.item import Item
from ..models.stock import Stock
from ..models.vendor import Vendor
from ..models.customer import Customer

class Test_Interactions(unittest.TestCase):
        def setUp(self):
            self.customer = Customer("kevin",cutomer_money=10.00)
            stocks = {"chips" :Stock("chips",10,2.50),
                "soda":Stock("soda",40,1.50) }
            self.vendor = Vendor(stocks=stocks, accumulated_money =20.00 )

        def test_buy(self):
            customer = self.customer
            vendor = self.vendor

            money_sent = customer.take_money(5.00)
            item, ammout_leftover = vendor.give_item("chips",money_sent)
            if item != None:
                customer.add_item(item)

            if ammout_leftover != None:
                customer.add_money(ammout_leftover)

            self.assertEqual( ammout_leftover , 2.50)
            self.assertEqual( item.name , "chips" )
            self.assertEqual( customer.cutomer_money , 7.50  )
            self.assertEqual( vendor.accumulated_money , 22.50  )


        def test_restock(self):
            vendor = self.vendor
            newstock1 = Stock("candy",5,1.00)
            newstock2 = Stock("soda",5,1.50)

            vendor.stock_vendor(newstock1)
            vendor.stock_vendor(newstock2)

            self.assertEqual(vendor.stocks['candy'].quantity , 5)
            self.assertEqual(vendor.stocks['soda'].quantity , 45)









