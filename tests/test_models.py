import unittest

from ..models.item import Item
from ..models.stock import Stock
from ..models.vendor import Vendor
from ..models.customer import Customer

class Test_Item(unittest.TestCase):
     def test_init_item(self):
        Item("chip")

class Test_Stock(unittest.TestCase):
    def test_init_stock(self):
        Stock("chips",10,2.50)
    

class Test_Vendor(unittest.TestCase):
    def test_init_vendor(self):
        stocks = {"chips" :Stock("chips",10,2.50),
                "soda":Stock("soda",40,1.50) }
        starting_money = 10.10

        Vendor(stocks=stocks, accumulated_money =starting_money )

class Test_Customer(unittest.TestCase):
    def test_init_customer(self):
        money = 20.50
        Customer("kevin",cutomer_money=money)
    
