from .stock import Stock
from .item import Item
from typing import Union,List, Dict, Tuple

class Vendor:
    def __init__(self, stocks:Dict[str,Stock] = {} , accumulated_money: float = 0.0):
        self.stocks: Dict[str,Stock]  = stocks
        self.accumulated_money: float = accumulated_money

    def get_stock_names(self) -> Union[None,List[str]]:
        if len(self.stocks) == 0:
            return []

        output:List[str] = [] 

        for stock in self.stocks.values():
            output.append(stock.name)

        return output

    def get_stock_names_prices(self) -> Dict[str,float]:
        if len(self.stocks ) == 0:
            return {}

        output:Dict[str,float] = {}

        for stockname, stock in self.stocks.items():
            if stock.quantity != 0:
                output[stockname] = stock.price
        
        return  output


    def give_item(self, name:str, ammount_given: float) -> Tuple[Union[None, Item],float] :
        if name not in self.stocks:
            return None, ammount_given

        if self.stocks[name].quantity == 0:
            return None, ammount_given


        price = self.stocks[name].price

        if ammount_given < price:
            return None, ammount_given
  
        item:Item = self.stocks[name].give_item()
        ammout_leftover = ammount_given - price
        self.accumulated_money = self.accumulated_money + price

        return  item, ammout_leftover


    def stock_vendor(self, stock: Stock) -> None:
        
        if stock.name not in self.stocks:
            self.stocks[stock.name] = stock
        else:
            self.stocks[stock.name].quantity = self.stocks[stock.name].quantity + stock.quantity

    def change_stock_price(self, name:str, newprice:float):
        if name in self.stocks:
            self.stocks[name].change_price(newprice)
