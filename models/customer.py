from .item import Item
from typing import List, Union

class Customer:
    def __init__(self, cutomer_name:str , cutomer_money: float = 0.0):
        self.cutomer_name = cutomer_name
        self.cutomer_money = cutomer_money
        self.items_owned: List[Item] = []

    def add_money(self, new_money:Union[float,int]) -> None:
        self.cutomer_money = self.cutomer_money + new_money

    def take_money(self, ammount_extrated:Union[float,int]) -> Union[float,None]:
        if ammount_extrated < self.cutomer_money:
            self.cutomer_money = self.cutomer_money - ammount_extrated
            return float(ammount_extrated)
        else:
            return None

    def add_item(self,item:Item) -> None:
        self.items_owned.append(item)


