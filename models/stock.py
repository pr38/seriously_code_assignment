from .item import Item

class Stock:
    def __init__(self, name:str, quantity: int, price: float):
        self.name:str = name
        self.price:float = price
        self.quantity:int = quantity

    def give_item(self) -> Item:
        self.quantity = self.quantity -1

        return Item(self.name)

    def give_price(self) -> float:
        return self.price

    def change_price(self, newprice: float) -> None:
        self.price = newprice
